const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
const Dotenv = require('dotenv-webpack');

module.exports = {
  target: 'webworker',
  resolve: {
    alias: {
      fs: path.resolve(__dirname, './null.js'),
    },
    extensions: ['.ts', '.js', '.json', '.mjs', '.graphql'],
  },
  mode: 'production',
  module: {
    rules: [
      {
        // Include ts, tsx, js, and jsx files.
        test: /\.(ts|js|graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: [
            [
              '@babel/preset-env',
              {
                modules: false,
                loose: true,
                useBuiltIns: 'usage',
                corejs: 3,
                targets: {
                  browsers: 'last 10 Chrome versions',
                },
                exclude: [/web.dom/, /generator|runtime/],
              },
            ],
            '@babel/typescript',
          ],
          plugins: [
            '@babel/plugin-proposal-object-rest-spread',
            '@babel/plugin-proposal-class-properties',
            '@babel/plugin-proposal-optional-chaining',
            'import-graphql',
          ],
        },
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto',
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new LodashModuleReplacementPlugin(),
    new Dotenv(),
  ],
  // node: false,
  optimization: {
    // Minify the final script if we're deploying and our config allows it
    minimize: true,
    noEmitOnErrors: true,
    usedExports: true,
  },
};
