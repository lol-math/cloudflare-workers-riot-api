import GraphQLJSON from 'graphql-type-json';
import GraphQLLong from 'graphql-type-long';

export default {
  Query: {
    getSummonerById: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getSummonerById(args.id),
    getSummonerByAccountId: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getSummonerByAccountId(args.accountId),
    getSummonerByName: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getSummonerByName(args.name),
    getChampionMasteries: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getChampionMasteries(args.summonerId),
    getChampionMastery: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getChampionMastery(args.summonerId, args.championId),
    getChampionMasteryScore: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getChampionMasteryScore(args.summonerId),
    getLeagueEntries: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getLeagueEntries(args.summonerId),
    getMasterLeague: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getMasterLeague(args.queue),
    getChallengerLeague: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getChallengerLeague(args.queue),
    getLeague: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getLeague(args.leagueId),
    getActiveGame: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getActiveGame(args.summonerId),
    getFeaturedGames: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getFeaturedGames(),
    getMatch: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getMatch(args.matchId),
    getMatchList: (
      obj,
      {
        endTime,
        beginIndex,
        beginTime,
        champion,
        endIndex,
        queue,
        season,
        accountId,
      },
      { dataSources },
    ) =>
      dataSources.riotAPI.getMatchList(accountId, {
        endTime,
        beginIndex,
        beginTime,
        champion,
        endIndex,
        queue,
        season,
      }),
    getTimeline: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getTimeline(args.matchId),
    getStatus: (obj, args, { dataSources }) => {
      return dataSources.riotAPI.getStatus();
    },
    getChampionRotations: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getChampionRotations(),
    getThirdPartyCode: (obj, args, { dataSources }) =>
      dataSources.riotAPI.getThirdPartyCode(args.summonerId),
  },
  ChampionMastery: {
    summoner: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getSummonerById(parent.playerId),
  },
  Summoner: {
    championMasteries: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getChampionMasteries(parent.id),
    championMasteryScore: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getChampionMasteryScore(parent.id),
    activeGame: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getActiveGame(parent.id),
    thirdPartyCode: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getThirdPartyCode(parent.id),
    matchHistory: (
      parent,
      { endTime, beginIndex, beginTime, champion, endIndex, queue, season },
      { dataSources },
    ) =>
      dataSources.riotAPI.getMatchList(parent.accountId, {
        endTime,
        beginIndex,
        beginTime,
        champion,
        endIndex,
        queue,
        season,
      }),
    leagueEntries: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getLeagueEntries(parent.id),
  },
  LeagueEntry: {
    leagueList: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getLeague(parent.leagueId),
    summoner: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getSummonerById(parent.summonerId),
  },
  Participant: {
    summoner: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getSummonerByName(parent.summonerName),
  },
  Player: {
    summoner: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getSummonerById(parent.summonerId),
  },
  LeagueItem: {
    summoner: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getSummonerById(parent.playerOrTeamId),
  },
  MatchReference: {
    match: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getMatch(parent.matchId),
  },
  Match: {
    timeline: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getTimeline(parent.matchId),
  },
  CurrentGameParticipant: {
    championMasteries: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getChampionMasteries(parent.summonerId),

    championMasteryScore: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getChampionMasteryScore(parent.summonerId),

    summoner: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getSummonerById(parent.summonerId),

    leagueEntries: (parent, args, { dataSources }) =>
      dataSources.riotAPI.getLeagueEntries(parent.summonerId),
  },
  Long: GraphQLLong,
  JSON: GraphQLJSON,
};
