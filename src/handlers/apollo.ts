import { ApolloServer, GraphQLOptions } from 'apollo-server-cloudflare';
import { graphqlCloudflare } from 'apollo-server-cloudflare/dist/cloudflareApollo';
import { Request, Response, URL } from 'apollo-server-env';

import KVCache from '../kv-cache';
import resolvers from '../resolvers';
import typeDefs from '../schema.graphql';
import RiotAPI from '../datasources/RiotApi';

const dataSources = () => ({
  riotAPI: new RiotAPI(),
});

const kvCache = { cache: new KVCache() };

const createServer = (graphQLOptions: GraphQLOptions) =>
  new ApolloServer({
    typeDefs,
    resolvers,
    introspection: true,
    dataSources,
    engine: false,
    context: ({ request }: { request: Request }) => {
      // check from req
      const region = request.headers.get('region');
      return {
        region,
      };
    },
    ...(graphQLOptions.kvCache ? kvCache : {}),
  });

const handler = (request: Request, graphQLOptions: GraphQLOptions) => {
  const server = createServer(graphQLOptions);
  return graphqlCloudflare(() => server.createGraphQLServerOptions(request))(
    request,
  );
};

export default handler;
