class KVCache {
  get(key: string) {
    return WORKERS_GRAPHQL_CACHE.get(key);
  }

  set(key: string, value: string, options: any) {
    const opts = {};
    const ttl = options && options.ttl;
    if (ttl) {
      opts.expirationTtl = ttl;
    }
    return WORKERS_GRAPHQL_CACHE.put(key, value, opts);
  }
  delete(key: string) {
    return WORKERS_GRAPHQL_CACHE.delete(key);
  }
}

export default KVCache;
