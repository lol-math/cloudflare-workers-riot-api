import { RESTDataSource, RequestOptions } from 'apollo-datasource-rest';
import endpoints from '../data/endpoints.json';
import { pickBy } from 'lodash';

interface IMatchListFilters {
  endTime?: number;
  beginIndex?: number;
  beginTime?: number;
  champion?: number;
  endIndex?: number;
  queue?: number;
  season?: number;
}

export default class RiotAPI extends RESTDataSource {
  constructor() {
    super();
  }

  get baseURL() {
    // @ts-ignore
    return `https://${endpoints[this.context.region]}.api.riotgames.com`;
  }

  willSendRequest = (request: RequestOptions) => {
    request.headers.set('X-Riot-Token', process.env.RIOT_API_KEY || '');
  };

  getSummonerByName = (name: string) =>
    this.get(`lol/summoner/v4/summoners/by-name/${name}`);

  getSummonerById = (id: string) => this.get(`lol/summoner/v4/summoners/${id}`);

  getSummonerByAccountId = (accountId: string) =>
    this.get(`lol/summoner/v4/summoners/by-account/${accountId}`);

  getChampionMasteries = (summonerId: string) =>
    this.get(
      `lol/champion-mastery/v4/champion-masteries/by-summoner/${summonerId}`,
    );

  getChampionMastery = (summonerId: string, championId: number) =>
    this.get(
      `lol/champion-mastery/v4/champion-masteries/by-summoner/${summonerId}/by-champion/${championId}`,
    );

  getChampionMasteryScore = (summonerId: string) =>
    this.get(`lol/champion-mastery/v4/scores/by-summoner/${summonerId}`);

  getLeagueEntries = (summonerId: string) =>
    this.get(`lol/league/v4/entries/by-summoner/${summonerId}`);

  getLeague = (leagueId: string) =>
    this.get(`lol/league/v4/leagues/${leagueId}`);

  getMasterLeague = (queue: string) =>
    this.get(`lol/league/v4/masterleagues/by-queue/${queue}`);

  getChallengerLeague = (queue: string) =>
    this.get(`lol/league/v4/challengerleagues/by-queue/${queue}`);

  getActiveGame = (summonerId: string) =>
    this.get(`lol/spectator/v4/active-games/by-summoner/${summonerId}`);

  getFeaturedGames = () => this.get(`lol/spectator/v4/featured-games`);

  getMatch = (matchId: number) => this.get(`lol/match/v4/matches/${matchId}`);

  getTimeline = (matchId: number) =>
    this.get(`lol/match/v4/timelines/by-match/${matchId}`);

  getMatchList = (accountId: string, filters: IMatchListFilters) =>
    this.get(
      `lol/match/v4/matchlists/by-account/${accountId}`,
      pickBy(filters),
    );

  getChampionRotations = () => this.get(`lol/platform/v3/champion-rotations`);

  getStatus = () => this.get(`lol/status/v3/shard-data`);

  getThirdPartyCode = (summonerId: string) =>
    this.get(`lol/platform/v4/third-party-code/by-summoner/${summonerId}`);
}
